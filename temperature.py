def convert_far_to_cel(f):
    '''Converts temperature in Fahrenheit to Celsius '''    
    c = (f - 32) * 5 / 9
    return print(f'{f} degrees F = {c:.2f} degrees C')


def convert_cel_to_far(c):
    '''Converts temperature in Celsius to Fahrenheit '''
    f = c * 9 / 5 + 32    
    return print(f'{c} degrees C = {f:.2f} degrees F')


f = float(input('Enter a temperature in degres F: '))
convert_far_to_cel(f)

c = float(input('Enter a temperature in degres C: '))
convert_cel_to_far(c)





