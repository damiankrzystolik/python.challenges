import easygui as gui
from PyPDF2 import PdfReader, PdfWriter

# step 1
input_path = gui.fileopenbox(
    title='Choose a PDF file to rotate.',
    default='*.pdf',
)
# # step 2
if input_path is None: exit()

input_file = PdfReader(input_path)
total_pages = len(input_file.pages)

# step 3
starting_page_loop = True
while starting_page_loop is True:
        starting_page = gui.enterbox(f'Choose a starting page. From 1 to {total_pages}',title='PDF Page Extraction Application')
        # step 4
        if starting_page is None:
            exit()
        try:
            starting_page = int(starting_page)
            # step 5
            if starting_page < 1 or starting_page > total_pages:
                gui.msgbox('Please choose a number of page.', title='PDF Page Extraction Application')
                starting_page_loop = True
            else:
                starting_page_loop = False
        except ValueError:
            warning = gui.msgbox('Please enter a valid page number')
# step 6
ending_page_loop = True
while ending_page_loop is True:
        ending_page = gui.enterbox(f'Choose a ending page. From {starting_page + 1} to {total_pages}',title='PDF Page Extraction Application')
        # step 7
        if ending_page is None:
            exit()
        try:
            ending_page = int(ending_page)
            # step 8
            if ending_page < starting_page or ending_page > total_pages:
                gui.msgbox('Please choose a number of page.', title='PDF Page Extraction Application')
                ending_page_loop = True
            else:
                ending_page_loop = False
        except ValueError:
            warning = gui.msgbox('Please enter a valid page number')
# step 9
output_path = gui.filesavebox(default='*.pdf')
# step 10
if output_path is None:
    exit()
# step 11
while input_path == output_path:
    gui.msgbox('Cannot overwrite original fole.')
    output_path = gui.filesavebox()
    if output_path is None:
        exit()

# step 12
output_pdf = PdfWriter()
for n in range(starting_page - 1, ending_page):
    page = input_file.pages[n]
    output_pdf.add_page(page)

with open(output_path, 'wb') as output_file:
    output_pdf.write(output_file)




