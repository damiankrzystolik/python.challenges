import random

def region_1(chance_of_winning):
    if random.random() < chance_of_winning:
        return 1    # Win
    else:
        return 0    # Lose

def region_2(chance_of_winning):
    if random.random() < chance_of_winning:
        return 1    # Win
    else:
        return 0    # Lose

def region_3(chance_of_winning):
    if random.random() < chance_of_winning:
        return 1    # Win
    else:
        return 0    # Lose

def candidate_A_win():
    regions = region_1(.87) + region_2(.65) + region_3(.17)
    if regions >= 2:
        return 1    # wins the election
    else:
        return 0    # lose the elections

def simulate_elections(num_of_trials):
    percentage_wins_candidate_A = 0
    for i in range(num_of_trials):
        percentage_wins_candidate_A += candidate_A_win()
        average = round((percentage_wins_candidate_A / num_of_trials)*100,2)
    return average



print(simulate_elections(10_000))





