import random



capitals_dict = {
    'Alabama' : 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta'
}

a = True
while a == True:
    print('To end game, write "exit"')
    state, capital = random.choice(list(capitals_dict.items()))
    answer = input(f'   Capital of {state} is? ')
    answer = answer.lower()

    if answer == capital.lower():
        print("Correct")
        a = False

    elif answer == 'exit':
        print(f'Correct answer is "{capital}"')
        print('Goodbye')
        a = False

    else:
        print('Incorrect')

