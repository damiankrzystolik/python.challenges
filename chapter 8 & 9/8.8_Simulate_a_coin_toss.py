import random
def coin_flip():
    if random.randint(0,1) == 0:
        return 'heads'
    else:
        return 'tails'

def sequence():
    heads = 0
    tails = 0
    while heads < 1 or tails < 1:
        if coin_flip() == "heads":
            heads = heads + 1
        else:
            tails = tails + 1

    flips = tails + heads
    return flips


sum_of_flips = 0
trials = 10_000
for i in range(trials):
    sum_of_flips += sequence()
    average = sum_of_flips / trials
print(average)


