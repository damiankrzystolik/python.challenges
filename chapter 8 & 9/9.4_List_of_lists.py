import statistics

universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]
def enrollment_stats(universities):
    list1 = []
    for i in range(len(universities)):
        list1.append(universities[i][1])

    list2 = []
    for i in range(len(universities)):
        list2.append(universities[i][2])
    return [list1, list2]

def mean(x):
    mean = statistics.mean(x)
    return mean

def median(x):
    median = statistics.median(x)
    return median

print(f'''***********************************************************
Total students:  {sum(enrollment_stats(universities)[0]):,}
Total tuition: $ {sum(enrollment_stats(universities)[1]):,}

Student mean:   {mean(enrollment_stats(universities)[0]):,.2f}
Student median: {median(enrollment_stats(universities)[0]):,}

Tuition mean:   $ {mean(enrollment_stats(universities)[1]):,.2f}
Tuition median: $ {median(enrollment_stats(universities)[1]):,}
***********************************************************''')

