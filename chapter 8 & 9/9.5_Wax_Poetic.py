import random

nouns = ['fossil', 'horse', 'aardvark', 'judge', 'chef', 'mango', 'extrovert', 'gorilla']
verbs = ['kicks', 'jingles', 'bounces', 'slurps', 'meows', 'explodes', 'curdles']
adjectives = ['furry', 'balding', 'increduloud', 'fragrant', 'exuberant', 'glistering']
prepositions = ['against', 'after', 'into', 'beneath','upon', 'for', 'in', 'like', 'over', 'within']
adverbs = ['curiously', 'extravagantly', 'tantalizingly', 'furiously', 'sensuosly']
a = ['A', 'An']

aAn = random.choice(a)

noun1 = random.choice(nouns)
noun2 = random.choice(nouns)
noun3 = random.choice(nouns)

verb1 = random.choice(verbs)
verb2 = random.choice(verbs)
verb3 = random.choice(verbs)

adj1 = random.choice(adjectives)
adj2 = random.choice(adjectives)
adj3 = random.choice(adjectives)

prep1 = random.choice(prepositions)
prep2 = random.choice(prepositions)

adverb1 = random.choice(adverbs)

print(f'''{aAn} {adj1} {noun1}

{aAn} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}
{adverb1}, the {noun1} {verb2}
the {noun2} {verb3} {prep2} a {adj3} {noun3}''')