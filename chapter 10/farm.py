class Animal:
    def __init__(self, name, age, food):
        self.name = name
        self.age = age
        self.food = food
    def introduce_myself(self):
        print(f"I'm {self.name}")

    def eat(self):
        self.food += 1
        print(f"I ate {self.food} serving of food")

class Cow(Animal):

    def milk(self):
        if self.food < 3:
            print('I ate too little to give milk')
        else:
            print('I can give you milk now.')
            print('')
            self.food = 0

class Horse(Animal):

    def eat(self):
        if self.food == 0:
            print("I'm hungry")
            self.food += 1
        elif self.food <= 4:
            self.food += 1
            print("I'm still hungry")
        else:
            print("I'm full")
            self.food = self.food
        print(f"I ate {self.food} serving of food")

class Chicken(Animal):
    def __init__(self, name, age, food, wings, legs):
        super().__init__(name, age, food)
        self.wings = wings
        self.legs = legs

    def introduce_myself(self):
        super().introduce_myself()
        print(f'I have only {self.legs} legs but I have {self.wings} wings.')

def main():

    damian = Animal('Damian', 38, 0)
    cow = Cow('Cow', 5, 0)
    horse = Horse('Horse', 6, 0)
    chicken = Chicken('Chicken', 2, 0, 2, 2)

    damian.introduce_myself()
    print("I'm a farmer and I take care of my animals")
    print('')

    animals = [chicken, horse, cow]
    for animal in animals:
        if animal == cow:
            animal.introduce_myself()
            cow.milk()
            animal.eat()
            animal.eat()
            cow.milk()
            animal.eat()
            animal.eat()
            cow.milk()
            animal.eat()
            animal.eat()
            cow.milk()
        else:
            animal.introduce_myself()
            animal.eat()
            animal.eat()
            animal.eat()
            animal.eat()
            animal.eat()
            animal.eat()
            animal.eat()
        print("")

if __name__ == "__main__":
    main()