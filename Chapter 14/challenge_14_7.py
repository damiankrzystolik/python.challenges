from PyPDF2 import PdfWriter, PdfReader
from pathlib import Path

scrambled = Path.home() / 'Documents' / 'PYTHON'/'RealPython_PythonBasics'/'r14 pdf'/'ch14_materialy_z_ksiazki' /'practice_files' / 'scrambled.pdf'

pdf_reader = PdfReader(str(scrambled))
pdf_writer = PdfWriter()


l = []
for page in pdf_reader.pages:
    r = page['/Rotate']
    if r != 0: page.rotate(-r)
    text = page.extract_text()
    p = page
    l.append({'page_text':text, 'page':p})


l = sorted(l, key=lambda d: d['page_text'])
for item in l:
    page = item['page']
    pdf_writer.add_page(page)


with Path('ch14_materialy_z_ksiazki/scrambled_unrotatedd.pdf').open('wb') as file:
    pdf_writer.write(file)
