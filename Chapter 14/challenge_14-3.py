from PyPDF2 import PdfWriter, PdfReader
from pathlib import Path




class PdfFileSplitter():
    def __init__(self,pdf_path):
        self.pdf_path = Path.cwd() / pdf_path
        self.pdf = PdfReader(self.pdf_path)
        self.slice_1_writer = None
        self.slice_2_writer = None


    def split(self, breakpoint):
        self.slice_1_writer = PdfWriter()
        self.slice_2_writer = PdfWriter()
        slice_1_reader = self.pdf.pages[:breakpoint]
        slice_2_reader = self.pdf.pages[breakpoint:]

        for page in slice_1_reader:
            self.slice_1_writer.add_page(page)

        for page in slice_2_reader:
            self.slice_2_writer.add_page(page)


    def write(self, filename):

        slice_1_pdf_path = Path.cwd() / (filename + '_1.pdf')
        with slice_1_pdf_path.open(mode="wb") as file:
            self.slice_1_writer.write(file)

        slice_2_pdf_path = Path.cwd() / (filename + '_2.pdf')
        with slice_2_pdf_path.open(mode="wb") as file:
            self.slice_2_writer.write(file)



pdf_spliter = PdfFileSplitter('Pride_and_Prejudice.pdf')
pdf_spliter.split(100)
pdf_spliter.write('aaa')
