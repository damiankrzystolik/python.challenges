

def bubble_sort_with_flag(data):
    """Funkcja sortowania bąbelkowego z modyfikacją flagi - może skrócić sortowanie."""
    flag = False
    step = 1
    while not flag:
        for i in range(len(data) - 1):
            if data[i] > data[i+1]:
                temp = data[i]
                data[i] = data[i+1]
                data[i+1] = temp
                flag = True
        if flag == False:
            print(f'Dane posortowano w {step -1} kroku.')
            break
        else:
            flag = False
            print(f'Krok {step}: {data}')
            step += 1

def bubble_sort(data):
    """Sortowanie bąbelkowe wg definicji."""
    step = 1
    for j in range(len(data)-1):
        for i in range(len(data)-1):
            if data[i] > data[i + 1]:
                temp = data[i]
                data[i] = data[i + 1]
                data[i + 1] = temp
                print(f'Krok {step}: {data}')
                step += 1
    print(f'Wykonano {step - 1} kroków.')


data_abc = ['z','b','c','y','e','f','x','h','i','a']
data = [21, 21, 10, 8, 12, 3, 3, 7, 20, 13]
bubble_sort_with_flag(data_abc)
print('*********')
data_abc = ['z','b','c','y','e','f','x','h','i','a']
data = [21, 21, 10, 8, 12, 3, 3, 7, 20, 13]
bubble_sort(data_abc)