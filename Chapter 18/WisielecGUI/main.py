import tkinter as tk
from PIL import ImageTk, Image
import random


JEDZENIE_VALUES = "Pierogi Bigos Barszcz Gołąbki Kopytka Placki Rosół Pomidorowa Kiełbasa Flaczki Pulpety Krupnik Grzybowa Kaszanka Pączki Faworki Ogórkowa Naleśniki".split()
KRAJE_VALUES = "Polska Niemcy Francja Włochy Hiszpania Portugalia Szwecja Norwegia Finlandia Rosja Kanada Meksyk Brazylia Argentyna Chile Australia Anglia Irlandia Holandia Belgia Austria Szwajcaria Grecja Turcja Egipt Maroko Indie Japonia Chiny Korea Kolumbia Peru Islandia Czechy Słowacja Chorwacja Brazyliia Indie".split()
ZWIERZETA_VALUES = 'Mrówka Pawian Bóbr Nietoperz Niedźwiedź Borsuk Wielbłąd Kot Małża Kobra Puma Kojot Krowa Jeleń Pies Osioł Kaczka Orzeł Fretka Lis Żaba Koza Gęś Jastrząb Lew Jaszczurka Lama Małpa Łoś Mysz Traszka Wydra Sowa Panda Papuga Gołąb Pyton Zając Owca Szczur Kruk Nosorożec Łosoś Foka Rekin Baran Skunks Leniwiec Wąż Pająk Bocian Łabędź Tygrys Ropucha Pstrąg Indyk Żółw Łasica Wielbłąd Wilk Wombat Zebra Antylopa Bizon Bobr Dzik Fenek Gibbon Hipopotam Irbis Jaguar Kanarek Mewa Niedźwiedź Orangutan Piesek Rys Sarna Tapir'.split()
SPORTY_VALUES = "Koszykówka Siatkówka Tenis Bieganie Pływanie Kolarstwo Narciarstwo Zapasy Gimnastyka Wspinaczka Surfing Golf Rugby Hokej Boks Karate Curling Snowboard Skateboard Triathlon Żeglarstwo Strzelectwo Judo Badminton Kajakarstwo Wioślarstwo Jeździectwo Szermierka Łucznictwo Himalaizm Baseball Wakeboarding Rafting Sumo Jogging Paintball Szachy".split()
ROSLINY_VALUES = "Róża Tulipan Stokrotka Fiołek Magnolia Orchidea Lilak Kaktus Begonia Gerbera Chryzantema Storczyk Anemon Akacja Lawenda Oliwka Jaśmin Mirt Podbiał Cyprys Wawrzyn Ananas Eukaliptus Sosna Dąb Buk Palma Wiśnia Cebula Marchew Kapusta Ogórek Rzodkiew Groszek Sałata Kolendra Koper Szczypiorek Szalotka Burak Marchewka Rukola Kalafior Brokuły Bób Arbuz Dynia Pomidory Papryka Botwina Brukselka Szparagi Żurawina Porzeczka Malina Agrest Jagoda Orzech Naleśnik Burak Żurawina Ogórek".split()
INSTRUMENTY_VALUES = "Gitara Skrzypce Fortepian Flet Trąbka Saksofon Bęben Tamburyn Klawesyn Harfa Akordeon Banjo Mandolina Wiolonczela Kontrabas Marakasy Wibrafon Trójkąt Bandżo Bongos Cymbały Klarnet Klawikord Ukulele Cytra Bałałajka Harmonijka Kastaniety".split()
FILMY = "Avatar Terminator Joker Titanic Deadpool Matrix KingKong Rambo Jumanji Shrek Mulan Aladdin Casablanca Godzilla Rocky Spiderman Superman Fantomas Gladiator Tarzan Incepcja Siedem Dracula Batman MadMax Gladiator Zorro Hamlet Dekalog Znachor Psy Kiler Pokot Katyń".split()
PRZEDMIOTY = "Stół Krzesło Ławka Szafka Komoda Kanapa Fotel Biurko Lampka Lustro Dywan Obraz Doniczka Zegar Książka Poduszka Półka Koc Naczynie Zastawa Garnek Patelnia Noż Kieliszek Widelec Talerz Kubek Miseczka Szklanka Dzbanek Bielizna Ołówek Długopis Notes Taśma Linijka Klej Szkicownik Marker Spinacz Termos Prysznic Kompres Dywanik Wieszak Kreda Gumka Kartka".split()
CATEGORY = [{'Zwierzęta': ZWIERZETA_VALUES}, {'Kraj': KRAJE_VALUES}, {'Jedzenie': JEDZENIE_VALUES}, {'Sport': SPORTY_VALUES}, {'Roślina': ROSLINY_VALUES}, {'Instrument muzyczny': INSTRUMENTY_VALUES}, {'Tytuł filmu': FILMY}, {'Przedmiot': PRZEDMIOTY}]


def getPlayerGuess(alreadyGuessed, wpisz_literke, komunikat, wybrane_literki, lbl_word, secretWord, mistake, lbl_guessed):
    secret_word = secretWord
    blanks = lbl_word.cget('text')
    guess = wpisz_literke.get().upper()
    # mistakes = len(alreadyGuessed)
    if len(guess) != 1:
        komunikat['text'] = 'Proszę, podaj tylko jedną literę.'
        wpisz_literke.delete(0, tk.END)
    elif guess in alreadyGuessed:
        komunikat['text'] = 'Tę literę już podawałeś. Wpisz inną.'
        wpisz_literke.delete(0, tk.END)
    elif guess not in secret_word:
        alreadyGuessed.append(guess)
        wybrane_literki_label = ' '.join(alreadyGuessed)
        wybrane_literki['text'] = f'Wybrane literki: {wybrane_literki_label}'
        wpisz_literke.delete(0, tk.END)
        mistake['text'] = f'Stracone szanse: {len(alreadyGuessed)}'
    elif not guess.isalpha():
        komunikat['text'] = 'Proszę, podaj LITERĘ.'
        wpisz_literke.delete(0, tk.END)
    else:
        wpisz_literke.delete(0, tk.END)
        blanks_list = list(blanks)
        secretWord_list = list(secret_word)
        for i in range(len(secretWord_list)):
            if guess == secretWord_list[i]:
                komunikat['text'] = f'Wybrane literka -{guess} znajduje sie w szukanym słowie'
                temp = secretWord_list[i]
                secretWord_list[i] = blanks_list[i]
                blanks_list[i] = temp
                blanks = ''.join(blanks_list)
                lbl_word['text'] = blanks

    win_loose(blanks, alreadyGuessed)
    images(lbl_guessed, alreadyGuessed)

def images(lbl_guessed, alreadyGuessed):
    images = ['_1.jpg', '_2.jpg', '_3.jpg', '_4.jpg', '_5.jpg', '_6.jpg', '_7.jpg']
    if alreadyGuessed == []:
        pass
    else:
        img = Image.open(images[len(alreadyGuessed) - 1])
        img = img.resize((250, 250))  # Optional: resize image
        img = ImageTk.PhotoImage(img)
        lbl_guessed.config(image=img)
        lbl_guessed.image = img  # keep a reference!



def win_loose(blanks, alreadyGuessed):
    if '*' not in blanks:
        for widget in root.winfo_children():
            widget.grid_forget()
        win_lbl = tk.Label(root, text='Wygrałeś', font=('Arial', 24))
        win_lbl.grid(row=0, column=0, padx=5, pady=(50, 5))
        btn = tk.Button(root, text='Zagraj jeszcze raz', font=('Arial', 12), command=lambda: start_app())
        btn.grid(row=1, column=0, padx=5, pady=(5, 20))
    elif len(alreadyGuessed) == 7:
        for widget in root.winfo_children():
            widget.grid_forget()
        win_lbl = tk.Label(root, text='Przegrałes', font=('Arial', 24))
        win_lbl.grid(row=0, column=0, padx=5, pady=(50, 5))
        btn = tk.Button(root, text='Zagraj jeszcze raz', font=('Arial',12), command=lambda: start_app())
        btn.grid(row=1, column=0, padx=5, pady=(5, 20))

        img = Image.open('_7.jpg')
        img = img.resize((250, 250))  # Optional: resize image
        img = ImageTk.PhotoImage(img)
        lbl = tk.Label(root)
        lbl.grid(row=2, column=0, padx=5, pady=5)
        lbl.config(image=img)
        lbl.image = img


def random_category(categories):
    selected_category = random.choice(categories)
    selected_key = list(selected_category.keys())[0]
    selected_values = selected_category[selected_key]
    return selected_key, selected_values


def start_app():
    alreadyGuessed = []
    selected_category, selected_values = random_category(CATEGORY)
    secretWord = random.choice(selected_values).upper()
    for widget in root.winfo_children():
        widget.grid_forget()


    wybrane_literki = tk.Label(root, text='Wybrane litery:', font=('Arial', 12))
    wybrane_literki.grid(row=0, column=0, sticky=tk.EW)

    mistake = tk.Label(root, text='Stracone szanse: ', font=('Arial', 15))
    mistake.grid(row=5, column=0, sticky=tk.EW)

    wpisz_literke = tk.Entry(root)
    wpisz_literke.grid(row=1, column=0, padx=5, pady=5)

    guzik_enter = tk.Button(root, text='ENTER', font=('Arial',12), command=lambda: getPlayerGuess(
        alreadyGuessed,
        wpisz_literke,
        komunikat,
        wybrane_literki,
        lbl_word,
        secretWord,
        mistake,
        lbl_guessed))
    guzik_enter.grid(row=2, column=0, padx=5, pady=5)

    komunikat = tk.Label(root, text="", justify='center')
    # komunikat.columnconfigure(0, weight=1, minsize=500)
    komunikat.grid(row=3, column=0, padx=5, pady=5, sticky=tk.EW)

    frm_word = tk.Frame(root)
    frm_word.grid(row=4, column=0)

    lbl_category = tk.Label(frm_word, text=f'Kategoria:\n{selected_category}', font=('Arial', 14))
    lbl_category.grid(row=0, column=0, padx=5, pady=5)

    blanks = '*' * len(secretWord)

    lbl_word = tk.Label(frm_word, text=blanks, font=('Arial',24))
    lbl_word.grid(row=1, column=0, padx=5, pady=5)
    lbl_guessed = tk.Label(frm_word)
    lbl_guessed.grid(row=2, column=0, padx=5, pady=5)
    # lbl_ = tk.Label(frm_word, text=secretWord) # tu pokazuje się wylosowane słowo
    # lbl_.grid(row=3, column=0, padx=5, pady=5)


root = tk.Tk()
root.title('Hangman')
root.geometry('500x600')
root.resizable(False, False)
root.columnconfigure(0, weight=1, minsize=500)

start_lbl = tk.Label(root, text='Welcome to Hangman', font=('Arial', 24))
start_lbl.grid(row=0, column=0, padx=5, pady=(5, 40))
start_button = tk.Button(root, text="Start", font=('Arial',18), command=lambda: start_app())
start_button.grid(row=1, column=0, padx=5, pady=5)

root.mainloop()