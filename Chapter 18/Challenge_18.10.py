import tkinter as tk
import random
from tkinter.filedialog import asksaveasfilename
def get_word(row, column):
    entry = frame.grid_slaves(row, column)[0]
    txt = entry.get()
    words = txt.split(',')
    if row < 4 and len(words) < 3:
        return None
    elif row == 4 and txt == '':
        return None
    else:
        word = random.choice(words)
        return word

def genereted_text():
    label_with_generate_txt['text'] = f'''{random.choice(['A', 'An'])} {get_word(2,1)} {get_word(0,1)}

    {random.choice(['A', 'An'])} {get_word(2,1)} {get_word(0,1)} {get_word(1,1)} {get_word(3,1)} the {get_word(2,1)} {get_word(0,1)}
    {get_word(4,1)}, the {get_word(0,1)} {get_word(1,1)}
    the {get_word(0,1)} {get_word(1,1)} {get_word(3,1)} a {get_word(2,1)} {get_word(0,1)}'''
    if 'None' in label_with_generate_txt['text']:
        label_with_generate_txt['text'] = "Użyto zbyt małe ilosci słów do stworzenia wiersza.\nNależy wprowadzić przynajmniej 3 słowa."

def save_file():
    filepath = asksaveasfilename(defaultextension='txt', filetypes=[("Text Files", "*.txt"),("All Files", "*.*")])
    if not filepath:
        return
    with open(filepath, mode='w') as f:
        text = label_with_generate_txt['text']
        f.write(text)


window = tk.Tk()
window.title('Make your own poem!')
window.columnconfigure(0, minsize=500, weight=1)
window.rowconfigure(3, weight=2)
window.rowconfigure(1, minsize=150, weight=1)

label = tk.Label(window, text='Wpisz swoje ulubione słowa przedzielone przecinkami.\nPrzynajmniej 3 słowa w wierszach 1-4\nPrzynajmniej 1 w wierszu 5.')
label.grid(column=0, row=0)

frame = tk.Frame(window)
frame.grid(column=0, row=1,sticky='ew')
frame.columnconfigure(0, minsize=100, weight=1)
frame.columnconfigure(1, minsize=400, weight=10)

label_txt_list = ['Nouns', 'Verbs', 'Adjectives', 'Prepositions', 'Adverbs']
for i, txt in enumerate(label_txt_list):
    label = tk.Label(frame, text=f'{txt}:')
    label.grid(column=0, row=i, padx=5, sticky='e')

for i in range(0,5):
    entry = tk.Entry(frame)
    entry.grid(column=1, row=i,padx=5, sticky='ew')

btn = tk.Button(window, text='Generate', command=genereted_text)
btn.grid(column=0, row=2)

frame_with_poems = tk.Frame(window, borderwidth=5, relief=tk.SUNKEN)
frame_with_poems.columnconfigure(0, weight=1)
frame_with_poems.grid(column=0, row=3, padx=5, pady=5, sticky=tk.NSEW)

label_with_generate_txt = tk.Label(frame_with_poems, text='Tutaj bedzie twój wiersz \n\n\n\n\n\n')
label_with_generate_txt.grid(column=0, row=0, padx=5, pady=5, sticky=tk.NSEW)


btn_save = tk.Button(frame_with_poems, text='Zapisz do pliku', command=save_file)
btn_save.grid(column=0, row=1, padx=5, pady=5)

window.mainloop()
