# wisielec
# Damian K

# Wiem że ten kod nie do końca tak ma wyglądać.
# Za dużo if-ów. Brak możliwości zagrania jeszcze raz itd.

# Działa, narazie działa i musze od niego odpocząć.
# Kiedyś do niego wróce i poprawie.
# Poproszę tylko wskazówki co tu jest żle.


import random

sz1 = ('''  


           
           
                      
                      
          /\\             
         /  \\           
        /____\\     
''')
sz2 = ('''   
           |
           |
           |
           |
           |
           |           
          /\\             
         /  \\           
        /____\\     
''')
sz3 = ('''           _____
           |
           |
           |
           |
           |
           |           
          /\\             
         /  \\           
        /____\\ 
''')
sz4 = ('''   _____
   |    |
   |
   |
   |
   |
   |
___|___''')
sz5 = ('''   _____
   |    |
   |    O
   |   /|\\
   |    |
   |   / \\
   |
___|___''')

lista_slów = ['kogut', 'zebra', 'gepard', 'krowa',
              'kanarek', 'wieloryb', 'delfin', 'chomik', 'papuga', 'kameleon', 'jaszczurka']
slowo = random.choice(lista_slów)    


szubienica = 0
kratki = '#' * len(slowo)
kr = '#'

print('Gra Wisielec')
print(kratki)

while slowo != "#"*len(slowo):
    wpisana_litera = input('Wpisz literę: ')
    if slowo.find(wpisana_litera) == -1:
        szubienica = szubienica + 1
    if wpisana_litera != '':                                # Bez tego if-a po naciśnięciu 'Enter' wywalało program.
        for n in range(0, len(slowo) - 1):
            indeks_litery = slowo.find(wpisana_litera)
            if indeks_litery >= 0:
                slowo = slowo[:indeks_litery] + kr + slowo[indeks_litery + 1:]
                kratki = kratki[:indeks_litery] + wpisana_litera + kratki[indeks_litery + 1:]

    else:
        print('Coś namieszałeś, tracisz jedną szanse.')
        szubienica = szubienica + 1

    if szubienica == 0:                                      # Te if-y musze jakoś ogarnąć na spokojnie.
        print(kratki)
    if szubienica == 1:
        print(sz1)
        print(kratki)
    if szubienica == 2:
        print(sz2)
        print(kratki)
    if szubienica == 3:
        print(sz3)
        print(kratki)
    if szubienica == 4:
        print(sz4)
        print(kratki)
    if szubienica == 5:
        print(sz5)
        print(kratki)
        break

if slowo == "#"*len(slowo):
    print('Wygrałeś!!!')
print('Koniec gry')



