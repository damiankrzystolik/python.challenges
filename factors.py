num = int(input('Enter a positive integer: '))

for n in range(1, num + 1):
    modulo_result = num % n
    if modulo_result == 0:
        print(f'{n} is a factor of {num}')
    

