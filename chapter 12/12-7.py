from pathlib import Path
import csv
def score_int(row):
    row['score'] = int(row['score'])
    return row

scores = Path.cwd() / 'scores.csv'
scores_list = []

file = scores.open(mode='r', encoding='utf-8', newline='')
reader = csv.DictReader(file)
for row in reader:
    score_int(row)
    scores_list.append(row)

# nowa lista w krórej chciałbym mieć wybrane słowniki
# jednak nie lista a słownik - @jachulczyk
new_score_list = {}

for d in scores_list:
    name = d["name"]
    score = d["score"]
    if name not in new_score_list:
        new_score_list[name] = score
    if score > new_score_list[name]:
        new_score_list[name] = score

high_scores = Path.cwd() / 'high_scores.csv'

file = high_scores.open(mode='w', encoding='utf-8', newline='')
writer = csv.DictWriter(file, fieldnames=['name', 'high_score'])
writer.writeheader()

#   https://stackoverflow.com/questions/58513909/python-write-dictionary-to-csv-with-header
#   https://docs.python.org/3/library/csv.html#csv.DictWriter
for name in new_score_list:
    row = {"name": name, "high_score": new_score_list[name]}
    writer.writerow(row)

file.close()
